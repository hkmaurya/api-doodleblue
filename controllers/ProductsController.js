const mongoose = require('mongoose'),
  Product = mongoose.model('Product'),
  Q = require('q'),
  CommonHelper = require('../helpers/common'),
  Logger = require('../helpers/logger'),
  ProductHelper = require('../helpers/product'),
  productTransformer = require('../transformers/product');


const addProductByCSV = async(req, res) => {
  try {
      const file = req.file;
      if(!file) return res.boom.notFound(res.__('selectFile'));
      const { filename, destination } = file;
      const productObject = await ProductHelper.getProductObjectForCSV(file, req.body, req.auth.credentials);
      if(!productObject.productData) return res.boom.notFound(res.__('unableToReadCSV'));
      // insert multiple records
      let productUpdate = productObject.productData.map(product => ({
        updateOne: {
          filter: { slug: product.slug },
          update: {$set: product},
          upsert: true
        }
      }));
      await Product.bulkWrite(productUpdate);
      CommonHelper.removeFileFromFolder(filename, destination);
      res.status(200).send({ 
          success: true, 
          message: res.__('productAdded'), 
          data: null
      }); 
  } catch(err) {
      Logger.add('Error in add product by user', err);
      return res.boom.badRequest(err);
  }
};

const productList = async (req, res) => {
  try {
    let offset = req.query.page ? (req.query.page - 1) * req.query.limit : 0;
    let limit = req.query.limit ? parseInt(req.query.limit) : 10;
    let query = { };
    //search by title
    if (req.query.name) {
      if (!query.$and) {
        query.$and = [];
      }
      const name = CommonHelper.searchUserData(req.query.name);
      query.$and.push({ $or: [{ title: name }, { slug: name }] });
    }
    let sortBy = { created_at: -1 };
      if(req.query.sort) {
        sortBy = req.query.sort == 'created_at' ? { created_at: 1 } : req.query.sort == '-popularity' ? { orderCount: -1 } : req.query.sort == 'popularity' ? { orderCount: 1 } : { created_at: -1 }
    }
    let products = await Q.all([
      Product.countDocuments(query).exec(),
      Product.aggregate([{ $match: query }, { $sort: sortBy }]).allowDiskUse(true).skip(parseInt(offset)).limit(parseInt(limit))
    ]);
    const result = {
      success: true,
      message: res.__('productListing'),
      data: productTransformer.productList(products[1]),
      totalCount: products[0]
    };
    return res.status(200).send(result);
  } catch (err) {
    Logger.add('Product Listing Error ', err);
    return res.boom.badRequest(err);
  }
};


module.exports = {
  productList,
  addProductByCSV
};
