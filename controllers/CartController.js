/*******************************************************************************
 * Cart Controller
 ******************************************************************************/
'use strict';

const mongoose = require('mongoose'),
    Cart = mongoose.model('Cart'),
    ProductHelper = require('../helpers/product'),
    CommonHelper = require('../helpers/common'),
    Logger = require('../helpers/logger'),
    CartTransformer = require('../transformers/cart');

const addItemToCart = async (req, res) => {
    try {
        let { productId, quantity } = req.body;
        const { _id } = req.auth.credentials;
        quantity = CommonHelper.stringToNumber(quantity);
        const productDetails = await ProductHelper.getProductInfo(productId);
        if(!productDetails) return res.boom.notFound(res.__('invalidProductId'));
        if(CommonHelper.stringToNumber(productDetails.quantity) < quantity) return res.boom.notFound(res.__('invalidQuantity'));
        let cart = await Cart.findOne({ user: _id });
        if(cart) {
            //Remove default product
            cart.products = cart.products.filter(item => Boolean(item.product) === true);
            const indexFound = cart.products.findIndex(item => item.product == productId);
            if(indexFound !== -1) {
                cart.products[indexFound].quantity = quantity;
                cart.products[indexFound].price = CommonHelper.stringToNumber(productDetails.price);
                cart.products[indexFound].total = cart.products[indexFound].quantity * CommonHelper.stringToNumber(productDetails.price);
                cart.subTotal = cart.products.map(item => item.total).reduce((acc, next) => acc + next);
            } else {
                cart.products.push({
                    product: productId,
                    quantity: quantity,
                    price: CommonHelper.stringToNumber(productDetails.price),
                    total: CommonHelper.stringToNumber(CommonHelper.stringToNumber(productDetails.price) * quantity)
                });
                cart.subTotal = cart.products.map(item => item.total).reduce((acc, next) => acc + next);
            }
            await cart.save();
        } else {
            const cartData = {
                products: [{
                    product: productId,
                    quantity,
                    total: CommonHelper.stringToNumber(CommonHelper.stringToNumber(productDetails.price) * quantity),
                    price: productDetails.price
                }],
                user: _id,
                subTotal: CommonHelper.stringToNumber(CommonHelper.stringToNumber(productDetails.price) * quantity)
            }
            let cartInfo = new Cart(cartData);
            await cartInfo.save();
        }
        res.status(200).send({
            success: true,
            message: res.__('success'),
            data: null,
        });
    } catch (err) {
        Logger.add('Add item to cart Error ', err);
        return res.boom.badRequest(err);
    }
}

const myCart = async(req, res) => {
    try {
        const { _id } = req.auth.credentials;
        let language = CommonHelper.getCurrentLanguage(req.headers['accept-language']);
        let cart = await Cart.findOne({ user: _id }).populate('products.product');
        let result;
        if(cart) {
            result = {
                success: true,
                message: res.__('myCart'),
                data: await CartTransformer.cartDetails(cart, language)
            };
        } else {
            const cartData = {
                products: [{
                    product: null,
                    quantity: 1,
                    total: 0,
                    price: 0
                }],
                user: _id,
                subTotal: 0
            }
            let cartInfo = new Cart(cartData);
            let newCart = await cartInfo.save();
            result = {
                success: true,
                message: res.__('myCart'),
                data: await CartTransformer.cartDetails(newCart, language)
            };
        }
        return res.status(200).send(result);
    } catch (err) {
        Logger.add('my-cart listing Error ', err);
        return res.boom.badRequest(err);
    }
}

const removeItemToCart = async (req, res) => {
    try {
        let { productId } = req.body;
        const { _id } = req.auth.credentials;
        let cart = await Cart.findOne({ user: _id });
        if(!cart) return res.boom.notFound(res.__('invalidCart'));
        let delProduct = cart.products.find(item => CommonHelper.objectToString(item.product) == CommonHelper.objectToString(productId));
        if(!delProduct) return res.boom.notFound(res.__('invalidProductId'));
        cart.products = cart.products.filter(item => CommonHelper.objectToString(item.product) != CommonHelper.objectToString(productId));
        cart.subTotal = CommonHelper.stringToNumber(cart.subTotal) - CommonHelper.stringToNumber(delProduct.total)
        await cart.save();
        res.status(200).send({
            success: true,
            message: res.__('success'),
            data: null,
        });
    } catch (err) {
        Logger.add('Remove item to cart Error ', err);
        return res.boom.badRequest(err);
    }
}

module.exports = {
    addItemToCart,
    myCart,
    removeItemToCart
};