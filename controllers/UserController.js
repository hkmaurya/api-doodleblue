/*******************************************************************************
 * User Controller
 ******************************************************************************/
 'use strict';
 const mongoose = require('mongoose'),
   User = mongoose.model('User'),
   Product = mongoose.model('Product'),
   Logger = require('../helpers/logger'),
   UserHelper = require('../helpers/user'),
   Q = require('q'),
   CommonHelper = require('../helpers/common'),
   UserTransformer = require('../transformers/user');
 
 const signup = async (req, res) => {
   try {
     const { email } = req.body;
     const isValid = CommonHelper.isEmailValid(email)
     if (!isValid) return res.boom.conflict(res.__('InvalidEmail'));
     const userInfo = await UserHelper.emailExist(email);
     if (userInfo) return res.boom.conflict(res.__('EmailTaken'));
     const userObject = UserHelper.getUserObject(req.body, req.headers['accept-language']);
     const user = new User(userObject);
     user.emailVerifyToken = UserHelper.getAuthTokens(user);
     const userData = await user.save();
     // Send signup mail
     await UserHelper.sendSignupMail(userData);
     res.status(200).send({
       success: true,
       message: res.__('SignupSuccess'),
       data: null,
     });
   } catch (err) {
     return res.boom.badRequest(err);
   }
 };
 
 const userLogin = async (req, res) => {
   try {
     const { email, password } = req.body;
     const user = await UserHelper.emailExist(email);
     if (!user) return res.boom.notFound(res.__('InvalidLogin'));
     const isMatchPassword = UserHelper.comparePassword(password, user.password);
     if (!isMatchPassword) return res.boom.unauthorized(res.__('InvalidLogin'));
     const userInfo = await user.save();
     res.status(200).send({
       success: true,
       token: UserHelper.getAuthTokens(userInfo),
       message: res.__('LoginSuccess'),
       data: UserTransformer.userloginDetails(userInfo),
     });
   } catch (err) {
     return res.boom.badRequest(err);
   }
 };

 const getUsersList = async (req, res) => {
  try {
    let offset = req.query.page ? (req.query.page - 1) * req.query.limit : 0;
    let limit = parseInt(req.query.limit ? req.query.limit : 10);
    let query = { };
    if (req.query.name) {
      if (!query.$and) {
        query.$and = [];
      }
      const name = CommonHelper.searchUserData(req.query.name);
      query.$and.push({ $or: [{ fullName: name }] });
    }
    let sortBy = { created_at: -1 };
      if(req.query.sort) {
        sortBy = req.query.sort == 'created_at' ? { created_at: 1 } : req.query.sort == '-mostPurchased' ? { orderCount: -1 } : req.query.sort == 'mostPurchased' ? { orderCount: 1 } : { created_at: -1 }
    }
    let users = await Q.all([User.countDocuments(query).exec(), User.find(query).sort(sortBy).skip(parseInt(offset)).limit(parseInt(limit))]);
    const result = {
      success: true,
      message: res.__('UserListing'),
      data: UserTransformer.userList(users[1]),
      totalCount: users[0],
    };
    return res.status(200).send(result);
  } catch (err) {
    Logger.add('Users Listing Error ', err);
    return res.boom.badRequest(err);
  }
};
 
 module.exports = {
   signup,
   userLogin,
   getUsersList
 };
 