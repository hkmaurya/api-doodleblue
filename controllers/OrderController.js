/*******************************************************************************
 * Order Controller
 ******************************************************************************/
 'use strict';
 const mongoose = require('mongoose'),
     OrderHelper = require('../helpers/order'),
     Order = mongoose.model('Order'),
     Cart = mongoose.model('Cart'),
     Product = mongoose.model('Product'),
     User = mongoose.model('User'),
     Q = require('q'),
     CommonHelper = require('../helpers/common'),
     Logger = require('../helpers/logger'),
     OrderTransformer = require('../transformers/order'),
     CartTransformer = require('../transformers/cart');

const placeOrder = async (req, res) => {
    try {
        let user = req.auth.credentials._id;
        let myCart = await Cart.findOne({ user }).populate('products.product');
        if(!myCart) return res.boom.notFound(res.__('invalidCart'));
        //Check cart's product qty
        if(myCart && myCart.products.length == 0 ) res.boom.notFound(res.__('invalidCartProduct'));
        //Check product qty
        let unavailableProducts = myCart.products.map(async item => {
            if(item.quantity > item.product.quantity) {
                return await CartTransformer.getProductDetails(item.product, item);
            }
        });
        unavailableProducts = await Promise.all(unavailableProducts);
        unavailableProducts = CommonHelper.removeFalseValueFromArray(unavailableProducts)
        if(unavailableProducts && unavailableProducts.length > 0) return res.boom.notFound(res.__('insufficientQty'), unavailableProducts);
        let order = new Order();
        order.orderId = await OrderHelper.getOrderAutoId();
        order.user = user;
        order.subTotal = myCart.subTotal;
        order.total = myCart.subTotal;
        order.products = await OrderHelper.getProducts(myCart.products);
        order.userInfo = OrderHelper.getUserInfo(req.auth.credentials);
        order.status = 2;
        await order.save();
        //Update product Quantity
        myCart.products.map(async(product) => {
            let getProduct = await Product.findById(product.product);
            if(getProduct && getProduct.quantity > 0) {
                getProduct.quantity -= product.quantity;
                getProduct.orderCount += product.quantity;
                await getProduct.save();
            }
        });
        //Update user order number
        let usr1 = await User.findById(user);
        if(usr1) {
          const sumTotal = arr => arr.reduce((sum, { quantity }) => sum + quantity, 0)
          usr1.orderCount += sumTotal(myCart.products);
          await usr1.save();
        }
         //Update my cart
         myCart.subTotal = 0;
         myCart.products = [];
         await myCart.save();
        return res.status(200).send({ success: true, message: res.__('orderPlaced'), data: null });
    } catch(err) {
        Logger.add('Error in place order by user', err);
        return res.boom.badRequest(err);
    }
};

const orderList = async (req, res) => {
    try {
      let offset = req.query.page ? (req.query.page - 1) * req.query.limit : 0;
      let limit = req.query.limit ? parseInt(req.query.limit) : 10;
      let query = { user: CommonHelper.stringToObjectId(req.auth.credentials._id) };
      //search by title
      if (req.query.name) {
        if (!query.$and) {
          query.$and = [];
        }
        const name = CommonHelper.searchUserData(req.query.name);
        query.$and.push({ $or: [{ 'userInfo.name': name }, { 'userInfo.email': name }, { products: { $elemMatch: { slug: name }}}] });
      }
      let sortBy = { created_at: -1 };
      if(req.query.sort) {
        sortBy = req.query.sort == 'created_at' ? { created_at: 1 } : req.query.sort == '-orderId' ? { orderId: -1 } : req.query.sort == 'orderId' ? { orderId: 1 } : { created_at: -1 }
      }
      let orders = await Q.all([
        Order.countDocuments(query).exec(),
        Order.aggregate([{ $match: query }, { $sort: sortBy }]).allowDiskUse(true).skip(parseInt(offset)).limit(parseInt(limit))
      ]);
      const result = {
        success: true,
        message: res.__('orderListing'),
        data: OrderTransformer.orderList(orders[1]),
        totalCount: orders[0]
      };
      return res.status(200).send(result);
    } catch (err) {
      Logger.add('Order Listing Error ', err);
      return res.boom.badRequest(err);
    }
  };

module.exports = {
    placeOrder,
    orderList
};