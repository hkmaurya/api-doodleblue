/*******************************************************************************
 * Cart Routes
 ******************************************************************************/
'use strict';
const CartController = require('../controllers/CartController'),
    auth = require('../helpers/auth').userValidate,
    validator = require('express-joi-validator'),
    CartSchema = require('../validations/cart-schema');

module.exports = (app) => {
    /**
   * @swagger
   *  /api/cart:
   *    post:
   *      summary: Add to Item in cart by User
   *      tags: [Cart]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/AddCart'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/AddCart'
   */
    app.post('/api/cart', validator(CartSchema.add), auth, (req, res) => {
        CartController.addItemToCart(req, res);
    });

    /**
   * @swagger
   *  /api/cart:
   *    get:
   *      summary: My-cart listing by user
   *      tags: [Cart]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      responses:
   *        "200":
   *          description: Success
   */

    app.get('/api/cart', auth, (req, res) => {
        CartController.myCart(req, res);
    });

    /**
   * @swagger
   *  /api/cart:
   *    delete:
   *      summary: Remove to Item in cart by User
   *      tags: [Cart]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/RemoveCart'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/RemoveCart'
   */
    app.delete('/api/cart', validator(CartSchema.remove), auth, (req, res) => {
        CartController.removeItemToCart(req, res);
    });
}