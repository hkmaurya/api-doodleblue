/*******************************************************************************
 * Product Routes
 ******************************************************************************/
 'use strict';

 const ProductController = require('../controllers/ProductsController'),
   auth = require('../helpers/auth').userValidate,
   validator = require('express-joi-validator'),
   multer = require('multer'),
   Config = require('../config/config'),
   common = require('../helpers/common'),
   ProductSchema = require('../validations/product-schema');

  let csvStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, Config.staticPaths.uploadFile);
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + "-" + Date.now() + "."+common.getFileExtension(file.originalname));
    }
  });

  let csvUpload = multer({
    storage: csvStorage,
    limits: { fileSize: Config.staticPaths.csvMaxSize }
  });
 
 module.exports = (app) => {
   /**
     * @swagger
     *  /api/products:
     *    get:
     *      summary: product list by user
     *      tags: [Product]
     *      security:
     *        - bearerAuth: []
     *      parameters:
     *        - in: header
     *          name: accept-language
     *          schema:
     *              type: string
     *              enum: [en, it]
     *          required: true
     *        - in: query
     *          name: name
     *          description: Product's name/title for search
     *          schema:
     *              type: string
     *        - in: query
     *          name: sort
     *          description: Sort by created_at
     *          schema:
     *              type: string
     *              enum: [created_at, -created_at, popularity, -popularity]
     *        - in: query
     *          name: page
     *          description: Page number for pagination
     *          schema:
     *              type: number
     *        - in: query
     *          name: limit
     *          description: Limit for pagination
     *          schema:
     *              type: number
     *      responses:
     *        "200":
     *          description: Success
     *          content:
     *            application/json:
     *              schema:
     *                $ref: '#/components/schemas/List'
     */
   app.get('/api/products', validator(ProductSchema.list), auth, (req, res) => {
     ProductController.productList(req, res);
   });

  /**
    * @swagger
    *  /api/csv/product:
    *    post:
    *      summary: Add Product by CSV
    *      tags: [Product]
    *      security:
    *        - bearerAuth: []
    *      parameters:
    *        - in: header
    *          name: accept-language
    *          schema:
    *              type: string
    *              enum: [en, it]
    *          required: true
    *      requestBody:
    *        required: true
    *        content:
    *          multipart/form-data:
    *            schema:
    *              $ref: '#/components/schemas/AddProductByCSV'
    *      responses:
    *        "200":
    *          description: Success
    *          content:
    *            multipart/form-data:
    *              schema:
    *                $ref: '#/components/schemas/AddProductByCSV'
    */
   app.post('/api/csv/product', csvUpload.single("csv"), auth, (req, res) => {
    ProductController.addProductByCSV(req, res);
  });
  
 };