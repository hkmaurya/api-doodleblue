'use strict';
const UserController = require('../controllers/UserController');
const auth = require('../helpers/auth').userValidate;
const validator = require('express-joi-validator');
const UserSchema = require('../validations/user-schema');
const Config = require('../config/config');

module.exports = (app) => {
  /**
   * @swagger
   *  /api/user/signup:
   *    post:
   *      summary: User Signup
   *      tags: [User]
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/UserSignup'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/UserSignup'
   */

  app.post('/api/user/signup', validator(UserSchema.signup), (req, res) => {
    UserController.signup(req, res);
  });

  /**
   * @swagger
   *  /api/user/login:
   *    post:
   *      summary: User Login
   *      tags: [User]
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/UserLogin'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/UserLogin'
   */
  app.post('/api/user/login', validator(UserSchema.Userlogin), (req, res) => {
    UserController.userLogin(req, res);
  });

  /**
   * @swagger
   *  /api/users:
   *    get:
   *      summary: Users List
   *      tags: [User]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *        - in: query
   *          name: name
   *          description: Name for search
   *          schema:
   *              type: string
   *        - in: query
   *          name: sort
   *          description: Sort by created_at
   *          schema:
   *              type: string
   *              enum: [created_at, -created_at, mostPurchased, -mostPurchased]
   *      responses:
   *        "200":
   *          description: Success
   */
   app.get('/api/users', auth, (req, res) => {
    UserController.getUsersList(req, res);
  });

};
