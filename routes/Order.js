/*******************************************************************************
 * Order Routes
 ******************************************************************************/
 'use strict';

 const OrderController = require('../controllers/OrderController'),
     auth = require('../helpers/auth').userValidate,
     validator = require('express-joi-validator'),
     OrderSchema = require('../validations/order-schema');
 
 module.exports = (app) => {
     /**
    * @swagger
    *  /api/order/place:
    *    post:
    *      summary: Order place by User
    *      tags: [Order]
    *      security:
    *        - bearerAuth: []
    *      parameters:
    *        - in: header
    *          name: accept-language
    *          schema:
    *              type: string
    *              enum: [en, it]
    *          required: true
    *      responses:
    *        "200":
    *          description: Success
    */
     app.post('/api/order/place', auth, (req, res) => {
        OrderController.placeOrder(req, res);
     });

     /**
     * @swagger
     *  /api/orders:
     *    get:
     *      summary: Order list by user
     *      tags: [Order                    ]
     *      security:
     *        - bearerAuth: []
     *      parameters:
     *        - in: header
     *          name: accept-language
     *          schema:
     *              type: string
     *              enum: [en, it]
     *          required: true
     *        - in: query
     *          name: name
     *          description: Product's name/User's name for search
     *          schema:
     *              type: string
     *        - in: query
     *          name: sort
     *          description: Sort by created_at
     *          schema:
     *              type: string
     *              enum: [created_at, -created_at, orderId, -orderId]
     *        - in: query
     *          name: page
     *          description: Page number for pagination
     *          schema:
     *              type: number
     *        - in: query
     *          name: limit
     *          description: Limit for pagination
     *          schema:
     *              type: number
     *      responses:
     *        "200":
     *          description: Success
     *          content:
     *            application/json:
     *              schema:
     *                $ref: '#/components/schemas/List'
     */
    app.get('/api/orders', auth, (req, res) => {
        OrderController.orderList(req, res);
    });
 }