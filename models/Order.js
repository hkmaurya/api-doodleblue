/*******************************************************************************
 * Order Model
 ******************************************************************************/
'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    timestamps = require('mongoose-timestamps');

//Define user sub-schema
const userSchema = new Schema({
    name: { type: String, default: '' },
    email: { type: String, default: '' }
});

//Define product sub-schema
const productSchema = new Schema({
    productId: { type: String, default: '' },
    title: { type: String, default: '' },
    slug: { type: String, default: '' },
    shortDescription: { type: String, default: '' },
    description: { type: String, default: '' },
    maxQuantity: { type: Number, default: 0 },
    quantity: { type: Number, default: 0 },
    price: { type: Number, default: 0 },
    total: { type: Number, default: 0 },
});
//Define Order schema
const OrderSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'User', required: true },
    orderId: { type: String, default: '' },
    userInfo: { type: userSchema },
    products: [productSchema],
    total: { type: Number, default: 0 },
    subTotal: { type: Number, default: 0 },
    status: { type: Number, default: 1, enum: [1, 2, 3] }, //1=> Payment initiated, 2=>Success, 3=> rejected
});

 //middle ware in serial
 OrderSchema.pre('save', (next) => {
    this.updated_at = Date.now();
    next();
});

 // Add timestamp plugin
 OrderSchema.plugin(timestamps, { index: true });
module.exports = mongoose.model('Order', OrderSchema);