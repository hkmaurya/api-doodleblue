/*******************************************************************************
 * Cart Model
 ******************************************************************************/
'use strict';
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    timestamps = require('mongoose-timestamps');

// Define Item sub-schema
const itemSchema = new Schema({
    product: { type: mongoose.Schema.Types.ObjectId, ref: "Product" },
    quantity: { type: Number, required: true, min: [1, 'Quantity can not be less then 1.'] },
    total: { type: Number, default: 0 },
    price: { type: Number, default: 0 }
});

//Define Cart schema
const CartSchema = new Schema({
    products: [itemSchema],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    subTotal: {
        type: "Number",
        default: 0
    }
});

//middle ware in serial
CartSchema.pre('save', (next) => {
    this.updated_at = Date.now();
    next();
});

// Add timestamp plugin
CartSchema.plugin(timestamps, { index: true });
module.exports = mongoose.model('Cart', CartSchema);