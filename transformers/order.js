const orderList = (orders) => {
    return orders.map((order) => {
      let { _id, orderId, userInfo, products, subTotal, created_at } = order;
        return {_id, orderId, userInfo, products, subTotal, created_at } ;
    });
};

const productList = (products) => {
    return products.map((product) => {
        let { _id, title } = product;
          return {_id, title } ;
    });
}
    
module.exports = {
    orderList,
    productList
}