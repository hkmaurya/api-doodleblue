const getProductDetails = async (product, data) => {
    return { _id: product._id, title: product.title, maxQuantity: product.quantity, quantity: data.quantity, price: data.price, total: data.total }
}

const getProducts = async (data) => {
    let result = data.map(async(p) => {
        return await getProductDetails(p.product, p);
    });
    return await Promise.all(result);
}

const cartDetails = async (data) => {
    let { _id, subTotal, products, updated_at } = data;
    return { _id, subTotal, products: subTotal > 0 ? await getProducts(products) : [], updated_at };
}

module.exports = {
    cartDetails,
    getProductDetails
};