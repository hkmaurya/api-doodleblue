/*******************************************************************************
 * User Helper
 ******************************************************************************/

const mongoose = require('mongoose'),
  User = mongoose.model('User'),
  sendmail = require('sendmail')(),
  Config = require('../config/config'),
  commonHelper = require('./common'),
  Jwt = require('jsonwebtoken'),
  bcrypt = require('bcrypt');

const jwtTokenVerify = (data) => {
  return new Promise((resolve, reject) => {
    Jwt.verify(data, Config.key.privateKey, (err, decoded) => {
      if (err) {
        resolve(false);
      } else {
        resolve(decoded);
      }
    });
  });
};

const sendSignupMail = (user) => {
  sendmail({
    from: 'no-reply@test.com',
    to: user.email,
    subject: 'Confirm your registration with Test App',
    html: `<table style="padding: 20px;border: 1px solid #eee;">
    <tbody>
        <tr>
            <td colspan="4" class="lead-text" style="padding: 10px 0;font-weight: 600;font-size: 18px;text-align:left;font-family: 'Junge', serif !important;">
                Hello ${user.fullName},
            </td>
        </tr>
        <tr>
            <td colspan="4" style="padding: 0px 0;font-size: 18px;font-family: 'Junge', serif !important;">
              Thanks for your registration.
            </td>
        </tr>
    </tbody>
    <tfoot style="font-size: 14px;font-family: 'Junge',serif;">
        <tr>
            <td colspan="4" style="padding-top: 40px;font-family: 'Junge',serif;">
                <span style="color:#777676">Thanks</span><br />
                Test APP staff
            </td>
        </tr>
    </tfoot>
</table>`,
  }, function(err, reply) {
    console.log(err && err.stack);
    console.dir(reply);
});
};

const comparePassword = (password, hashedPassword) => {
  password = commonHelper.removeEmptySpace(password);
  hashedPassword = commonHelper.removeEmptySpace(hashedPassword);
  return bcrypt.compareSync(password, hashedPassword);
};

const hashPassword = (password) => {
  password = commonHelper.removeEmptySpace(password);
  return bcrypt.hashSync(password, Config.saltRounds);
};

const getAuthTokens = (user) => {
  const { _id, firstName, lastName, email, userType, userStatus } = user;
  const jwtdata = { _id, firstName, lastName, email, userType, userStatus };
  return Jwt.sign(jwtdata, Config.key.privateKey, {
    expiresIn: Config.key.tokenExpiry,
  });
};

const emailExist = async (email) => {
  try {
    email = commonHelper.toLowerCase(email);
    return User.findOne({ email })
      .then((user) => (user ? user : false))
      .catch((e) => true);
  } catch (err) {
    //Need to log
    return true;
  }
};

const getUserObject = (data, language) => {
  const { email, password, firstName, lastName, userType, acceptTermAndCondition } = data;
  return {
    email: commonHelper.toLowerCase(commonHelper.removeEmptySpace(email)),
    firstName,
    lastName,
    fullName: `${firstName} ${lastName}`,
    password: hashPassword(password)
  };
};

module.exports = {
  emailExist,
  getUserObject,
  getAuthTokens,
  sendSignupMail,
  hashPassword,
  comparePassword,
  jwtTokenVerify
};
