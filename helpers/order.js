/*******************************************************************************
 * Order Helper
 ******************************************************************************/

 const mongoose = require('mongoose'),
 Order = mongoose.model('Order'),
 ProductHelper = require('./product'),
 Config = require('../config/config'),
 Logger = require('./logger'),
 CommonHelper = require('./common');

const getOrderAutoId = async () => {
    let order = await Order.findOne({}, {}, { sort: { created_at : -1 }});
    if(order) {
        return CommonHelper.stringToNumber(order.orderId) + 1 ;
    } else {
        return 1000;
    }
};

const getProducts = async (products) => {
    let results = products.map(async product => {
       let { _id, title, slug, shortDescription, description, quantity } = product.product;
        return {
            productId: _id,
            title,
            slug,
            shortDescription,
            description,
            maxQuantity: quantity,
            quantity: product.quantity,
            price: product.price,
            total: product.total
        };
    });
    return await Promise.all(results);
};

const getUserInfo = (data) => {
    let { firstName, lastName, email } = data ? data : { firstName: '', lastName: '', email: '' };
    return { name: `${firstName} ${lastName}`, email };
}

module.exports = {
    getOrderAutoId,
    getProducts,
    getUserInfo
}