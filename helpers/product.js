const CommonHelper = require('./common');
const Config = require('../config/config');
const csvToJSON = require("csvtojson");
const request = require('request');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');


const getProductId = data => {
  let id = CommonHelper.removeEmptySpace(data);
  return CommonHelper.isObjectIds(id) ? CommonHelper.stringToObjectIds(id) : '';
}

const getProductObjectForCSV = async (file, data) => {
  const { filename, destination } = file;
  const csvPath = Config.staticPaths.file + filename;
  const { titleName, shortDescriptionName, descriptionName, priceName, quantityName } = data;
  try {
      const jsonArray = await csvToJSON().fromStream(request.get(csvPath));
      if(jsonArray && jsonArray.length > 0) {
          let productData = jsonArray.map(x => {
              return {
                  title: x[titleName],
                  slug: CommonHelper.getSlugData(x[titleName]),
                  shortDescription: x[shortDescriptionName],
                  description: x[descriptionName],
                  price: x[priceName],
                  quantity: x[quantityName],
                  created_at: new Date(),
                  updated_at: new Date()
              }
          });
          productData = CommonHelper.removeFalseValueFromArray(productData);
          return { productData, total: jsonArray.length };
      } else {
          CommonHelper.removeFileFromFolder(filename, destination);
          return false;
      }

  } catch (err) {
      //Need to log
      console.error(err)
      return false;
  }
};

const getProductInfo = async (pId) => {
  try {
    let product = await Product.findById(pId);
    return product;
  } catch (err) {
    console.log(err, 'err in get detail of product');
    return false;
  }
};

module.exports = {
  getProductId,
  getProductObjectForCSV,
  getProductInfo
}
