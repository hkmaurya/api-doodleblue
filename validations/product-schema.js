const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

/**
 * @swagger
 *  components:
 *    schemas:
 *      List:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *            description: product's name/title for search
 *          sort:
 *            type: string
 *            description: product's sort by title etc
 *          page:
 *             type: number
 *          limit:
 *              type: number
 */
module.exports.list = {
  query: {
    name: Joi.string().optional().allow(''),
    sort: Joi.string().optional().allow(''),
    page: Joi.number(),
    limit: Joi.number(),
  }
};

/**
    * @swagger
    *  components:
    *    schemas:
    *      AddProductByCSV:
    *        type: object
    *        required:
    *          - csv
    *          - titleName
    *          - shortDescriptionName
    *          - descriptionName
    *          - priceName
    *          - quantityName
    *        properties:
    *          csv:
    *            type: string
    *            format: binary
    *            contentType: image/png, image/jpeg, text/html, application/csv
    *            description: CSV which you want to upload
    *          titleName:
    *            type: string
    *            description: Title column name
    *          shortDescriptionName:
    *            type: string
    *            description: Short-description column name
    *          descriptionName:
    *            type: string
    *            description: Description column name
    *          priceName:
    *            type: string
    *            description: Price column name
    *          quantityName:
    *            type: string
    *            description: Quantity column name
*/
module.exports.addProductByCSV = {
  body: {
      csv: Joi.string().required(),
      titleName: Joi.string().required(),
      shortDescriptionName: Joi.string().required(),
      descriptionName: Joi.string().required(),
      priceName: Joi.string().required(),
      quantityName: Joi.string().required()
  }
};

/**
   * @swagger
   *  components:
   *    schemas:
   *      ProductUpdate:
   *        type: object
   *        required:
   *          - title
   *          - shortDescription
   *          - description
   *          - price
   *          - quantity
   *        properties:
   *          title:
   *            type: string
   *            description: title for product.
   *          description:
   *            type: string
   *            description: description of the product.
   *          shortDescription:
   *            type: string
   *            description: shortDescription for the product
   *          price:
   *            type: number
   *            description: price for the product.
   *          quantity:
   *            type: number
   *            description: quantity of the product
*/
module.exports.update = {
  body: {
    title: Joi.string().required(),
    shortDescription: Joi.string().optional().allow(''),
    description: Joi.string().optional().allow(''),
    price: Joi.number().required(),
    quantity: Joi.number().required()
  }
};