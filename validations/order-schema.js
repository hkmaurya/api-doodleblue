const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

/**
 * @swagger
 *  components:
 *    schemas:
 *      PlaceOrder:
 *        type: object
 */