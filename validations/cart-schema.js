const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

/**
 * @swagger
 *  components:
 *    schemas:
 *      AddCart:
 *        type: object
 *        required:
 *          - productId
 *          - quantity
 *        properties:
 *          productId:
 *            type: string
 *            description: Product's _id.
 *          quantity:
 *            type: number
 *            description: Product's purchase quantity.
 */
module.exports.add = {
    body: {
        productId: Joi.string().required(),
        quantity: Joi.number().required().min(1)
    },
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      RemoveCart:
 *        type: object
 *        required:
 *          - productId
 *        properties:
 *          productId:
 *            type: string
 *            description: Product's _id.
 */
 module.exports.remove = {
    body: {
        productId: Joi.string().required()
    },
};